﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Cidenet.Entities.Domain;
using Cidenet.Database.StoreProcedures;
using Cidenet.Database.Conexion;
using Insight.Database;
using Cidenet.Contracts.Helpers;
using WebApplicationCidenet.ViewModels;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;

namespace WebApplicationCidenet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadosController : ControllerBase
    {
        private Cidenet_Empleado _repositorioEmpleados;

        public EmpleadosController()
        {
            _repositorioEmpleados = Conexion.Current().As<Cidenet_Empleado>();
        }

        [HttpGet("TipoIdentificacion")]
        public IEnumerable<GeneralViewModel> TipoIdentificacion()
        {
            List<GeneralViewModel> result = new List<GeneralViewModel>();
            try
            {
                result = _repositorioEmpleados.pr_TipoIdentificacion_Get();

                return result;
            }
            catch (Exception ex)
            {
                Operaciones.saveError(this, ex);
                return result;
            }

        }

        [HttpGet("Paises")]
        public IEnumerable<GeneralViewModel> Paises()
        {
            List<GeneralViewModel> result = new List<GeneralViewModel>();
            try
            {
                result = _repositorioEmpleados.pr_Paises_Get();

                return result;
            }
            catch (Exception ex)
            {
                Operaciones.saveError(this, ex);
                return result;
            }

        }

        [HttpGet("Areas")]
        public IEnumerable<GeneralViewModel> Areas()
        {
            List<GeneralViewModel> result = new List<GeneralViewModel>();
            try
            {
                result = _repositorioEmpleados.pr_Areas_Get();

                return result;
            }
            catch (Exception ex)
            {
                Operaciones.saveError(this, ex);
                return result;
            }
        }

        [HttpGet("GetOne")]
        public IEnumerable<Empleados> GetOne(string cd_NumeroIdentificacion)
        {
            List<Empleados> result = new List<Empleados>();
            try
            {
                result = _repositorioEmpleados.pr_GetOne_Empleado(cd_NumeroIdentificacion: cd_NumeroIdentificacion);

                return result;
            }
            catch (Exception ex)
            {
                Operaciones.saveError(this, ex);
                return result;
            }

        }

        [HttpGet("All")]
        public PagedList<EmpleadosModel> All(
            int? PageIndex = 1,
            int? PageSize = 10,
            string PrimerApellido = null,
            string SegundoApellido = null,
            string PrimerNombre = null,
            string OtrosNombres = null,
            long? TipoIdentificacion = null,
            string NumeroIdentificacion = null,
            long? pais = null,
            string correo = null,
            bool? estado = null)
        {

            List<Empleados> result = new List<Empleados>();
            try
            {
                result = _repositorioEmpleados.pr_Empleados_Get(IVch_nm_PrimerApellido: PrimerApellido,
                                                    IVch_nm_SegundoApellido: SegundoApellido,
                                                    IVch_nm_PrimerNombre: PrimerNombre,
                                                    IVch_nm_OtrosNombres: OtrosNombres,
                                                    IIntId_TipoIdentificacion: TipoIdentificacion,
                                                    IVch_cd_NumeroIdentificacion: NumeroIdentificacion,
                                                    IInt_Id_Pais: pais,
                                                    IVch_nm_CorreoElectronico: correo,
                                                    Ib_ind_Estado: estado,
                                                    PageNumber: PageIndex,
                                                    PageSize: PageSize).ToList();

                return new PagedList<EmpleadosModel>(
                    result.Select(t => new EmpleadosModel(t)),
                    PageIndex,
                    PageSize,
                    result.FirstOrDefault()?.TotalRecords ?? 0
                );
            }
            catch (Exception ex)
            {
                Operaciones.saveError(this, ex);

                return new PagedList<EmpleadosModel>(
                    result.Select(t => new EmpleadosModel(t)),
                    PageIndex,
                    PageSize,
                    result.FirstOrDefault()?.TotalRecords ?? 0
                );
            }


        }


        // POST: api/Empleados
        [HttpPost]
        public string Post([FromBody] Empleados empleado)
        {
            var result = "";
            try
            {

                result = _repositorioEmpleados.pr_Empleados_Create(IVch_nm_PrimerApellido: empleado.nm_PrimerApellido,
                                                                  IVch_nm_SegundoApellido: empleado.nm_SegundoApellido,
                                                                  IVch_nm_PrimerNombre: empleado.nm_PrimerNombre,
                                                                  IVch_nm_OtrosNombres: empleado.nm_OtrosNombres,
                                                                  IInt_Id_Pais: empleado.Id_Pais,
                                                                  IIntId_TipoIdentificacion: empleado.TipoIdentificacion,
                                                                  IVch_cd_NumeroIdentificacion: empleado.cd_NumeroIdentificacion,
                                                                  IDt_FechaIngreso: empleado.dt_FechaIngreso,
                                                                  IInt_Id_Area: empleado.Id_Area);
                return result;
            }
            catch (Exception ex)
            {
                Operaciones.saveError(this, ex);
                result = " No se pudo registrar al empleado, comuniquese con el administrador para revisión del log de errores";
                return result;

            }

        }

        // PUT: api/Empleados/5
        [HttpPut("{id}")]
        public string Put(int? id, [FromBody] Empleados empleado)
        {
            var result = "";

            try
            {
                result = _repositorioEmpleados.pr_Empleados_Update(IInt_Id_Empleados: id,
                                                             IVch_nm_PrimerApellido: empleado.nm_PrimerApellido,
                                                             IVch_nm_SegundoApellido: empleado.nm_SegundoApellido,
                                                             IVch_nm_PrimerNombre: empleado.nm_PrimerNombre,
                                                             IVch_nm_OtrosNombres: empleado.nm_OtrosNombres,
                                                             IInt_Id_Pais: empleado.Id_Pais,
                                                             IIntId_TipoIdentificacion: empleado.TipoIdentificacion,
                                                             IVch_cd_NumeroIdentificacion: empleado.cd_NumeroIdentificacion,
                                                             IDt_FechaIngreso: empleado.dt_FechaIngreso,
                                                             IInt_Id_Area: empleado.Id_Area);


                return result;
            }
            catch (Exception ex)
            {
                Operaciones.saveError(this, ex);

                result = " No se pudo actualizar al empleado, comuniquese con el administrador para revisión del log de errores";
                return result;
            }

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public string Delete(string id)
        {
            var result = "";

            try
            {
                result = _repositorioEmpleados.pr_Empleados_Delete(cd_NumeroIdentificacion: id);

                return result;
            }
            catch (Exception ex)
            {
                Operaciones.saveError(this, ex);

                result = " No se pudo eliminar al empleado, comuniquese con el administrador para revisión del log de errores";
                return result;
            }
        }

    }
}
