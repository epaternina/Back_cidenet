﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Cidenet.Entities.Domain;

namespace WebApplicationCidenet.ViewModels
{
    [DataContract(Name = "EmpleadosModel")]
    public class EmpleadosModel
    {
        public EmpleadosModel()
        {

        }

        public EmpleadosModel(Empleados Entity)
        {
            Id_Empleados = Entity.Id_Empleados;
            nm_PrimerApellido = Entity.nm_PrimerApellido;
            nm_SegundoApellido = Entity.nm_SegundoApellido;
            nm_OtrosNombres = Entity.nm_OtrosNombres;
            nm_PrimerNombre = Entity.nm_PrimerNombre;
            nm_Pais = Entity.nm_Pais;
            cd_TipoIdentificacion = Entity.cd_TipoIdentificacion;
            cd_NumeroIdentificacion = Entity.cd_NumeroIdentificacion;
            nm_CorreoElectronico = Entity.nm_CorreoElectronico;
            dt_FechaIngreso = Entity.dt_FechaIngreso;
            nm_Area = Entity.nm_Area;
            ind_Estado = Entity.ind_Estado;
            dt_FechaHoraRegistro = Entity.dt_FechaHoraRegistro;
            dt_Creado = Entity.dt_Creado;
        }


        [DataMember(Name = "Id_Empleados")]
        public long? Id_Empleados { get; set; }

        [DataMember(Name = "nm_PrimerApellido")]
        public string nm_PrimerApellido { get; set; }

        [DataMember(Name = "nm_SegundoApellido")]
        public string nm_SegundoApellido { get; set; }

        [DataMember(Name = "nm_OtrosNombres")]
        public string nm_OtrosNombres { get; set; }

        [DataMember(Name = "nm_PrimerNombre")]
        public string nm_PrimerNombre { get; set; }

        [DataMember(Name = "nm_Pais")]
        public string nm_Pais { get; set; }

        [DataMember(Name = "cd_TipoIdentificacion")]
        public string cd_TipoIdentificacion { get; set; }

        [DataMember(Name = "cd_NumeroIdentificacion")]
        public string cd_NumeroIdentificacion { get; set; }

        [DataMember(Name = "nm_CorreoElectronico")]
        public string nm_CorreoElectronico { get; set; }

        [DataMember(Name = "dt_FechaIngreso")]
        public DateTime? dt_FechaIngreso { get; set; }

        [DataMember(Name = "nm_Area")]
        public string nm_Area { get; set; }

        [DataMember(Name = "ind_Estado")]
        public bool ind_Estado { get; set; }

        [DataMember(Name = "dt_FechaHoraRegistro")]
        public DateTime? dt_FechaHoraRegistro { get; set; }

        [DataMember(Name = "dt_Creado")]
        public DateTime? dt_Creado { get; set; }
    }
}
