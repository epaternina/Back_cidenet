﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cidenet.Entities.Domain
{
    public class Empleados
    {
        public long? Id_Empleados { get; set; }
        public string nm_PrimerApellido { get; set; }
        public string nm_SegundoApellido { get; set; }
        public string nm_PrimerNombre { get; set; }
        public string nm_OtrosNombres { get; set; }
        public string nm_Pais { get; set; }
        public long? Id_Pais { get; set; }
        public string cd_TipoIdentificacion { get; set; }
        public long? TipoIdentificacion { get; set; }
        public string cd_NumeroIdentificacion { get; set; }
        public string nm_CorreoElectronico { get; set; }
        public DateTime? dt_FechaIngreso { get; set; }
        public string nm_Area { get; set; }
        public long? Id_Area { get; set; }
        public bool ind_Estado { get; set; }
        public DateTime? dt_FechaHoraRegistro { get; set; }
        public DateTime? dt_Creado { get; set; }
        public DateTime? dt_Modificado { get; set; }
        public int? TotalRecords { get; set; }
    }
}
