﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cidenet.Entities.Domain
{
    public class GeneralViewModel
    {
        public String Value { get; set; }
        public String Description { get; set; }
    }
}
