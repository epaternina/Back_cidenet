﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cidenet.Database.Conexion
{
    public class Conexion
    {
        public static DbConnection Current()
        {
            DbConnection DbConnection = new SqlConnection(ConnectionString());
            return DbConnection;
        }

        public static string ConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["DatabaseCidenet"]].ToString();
        }
    }
}
