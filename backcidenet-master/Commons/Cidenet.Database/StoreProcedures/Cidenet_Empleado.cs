﻿using Cidenet.Entities.Domain;
using Insight.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cidenet.Database.StoreProcedures
{
    public interface Cidenet_Empleado
    {
        [Sql("Cidenet_Empleado.pr_Empleados_Get", CommandType.StoredProcedure)]
        List<Empleados> pr_Empleados_Get(string IVch_nm_PrimerApellido = null,
                                         string IVch_nm_SegundoApellido = null,
                                         string IVch_nm_PrimerNombre = null,
                                         string IVch_nm_OtrosNombres = null,
                                         long? IIntId_TipoIdentificacion = null,
                                         string IVch_cd_NumeroIdentificacion = null,
                                         long? IInt_Id_Pais = null,
                                         string IVch_nm_CorreoElectronico = null,
                                         bool? Ib_ind_Estado = null,
                                         int? PageNumber = null,
                                         int? PageSize = null);

        [Sql("Cidenet_Maestro.pr_TipoIdentificacion_Get", CommandType.StoredProcedure)]
        List<GeneralViewModel> pr_TipoIdentificacion_Get();

        [Sql("Cidenet_Maestro.pr_Paises_Get", CommandType.StoredProcedure)]
        List<GeneralViewModel> pr_Paises_Get();

        [Sql("Cidenet_Maestro.pr_Areas_Get", CommandType.StoredProcedure)]
        List<GeneralViewModel> pr_Areas_Get();

        [Sql("Cidenet_Empleado.pr_Empleados_Create", CommandType.StoredProcedure)]
        string pr_Empleados_Create(string IVch_nm_PrimerApellido = null,
                                         string IVch_nm_SegundoApellido = null,
                                         string IVch_nm_PrimerNombre = null,
                                         string IVch_nm_OtrosNombres = null,
                                         long? IInt_Id_Pais = null,
                                         long? IIntId_TipoIdentificacion = null,
                                         string IVch_cd_NumeroIdentificacion = null,
                                         DateTime? IDt_FechaIngreso = null,
                                         long? IInt_Id_Area = null);

        [Sql("Cidenet_Empleado.pr_Empleados_Update", CommandType.StoredProcedure)]
        string pr_Empleados_Update(long? IInt_Id_Empleados= null,
                                      string IVch_nm_PrimerApellido = null,
                                      string IVch_nm_SegundoApellido = null,
                                      string IVch_nm_PrimerNombre = null,
                                      string IVch_nm_OtrosNombres = null,
                                      long? IInt_Id_Pais = null,
                                      long? IIntId_TipoIdentificacion = null,
                                      string IVch_cd_NumeroIdentificacion = null,
                                      DateTime? IDt_FechaIngreso = null,
                                      long? IInt_Id_Area = null);

        [Sql("Cidenet_Empleado.pr_Empleados_Delete", CommandType.StoredProcedure)]
        string pr_Empleados_Delete(string cd_NumeroIdentificacion = null);

        [Sql("Cidenet_Empleado.pr_GetOne_Empleado", CommandType.StoredProcedure)]
        List<Empleados> pr_GetOne_Empleado(string cd_NumeroIdentificacion = null);

    }
}
