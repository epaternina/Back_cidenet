USE [DataBaseCidenet]
GO

/****** Object:  UserDefinedFunction [Cidenet_Empleado].[fn_Correo_Get]    Script Date: 19/08/2021 5:09:19 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripci�n: Creaci�n de FN
*************************************************************************************
** BIT�CORA:
*************************************************************************************
** Fecha:          Autor:					Descripci�n:
** 2021-08-18	   Edinson Paternina R		Creaci�n Inicial
*************************************************************************************/
CREATE FUNCTION [Cidenet_Empleado].[fn_Correo_Get]
(
@IVch_nm_PrimerApellido VARCHAR(100) = NULL,
@IVch_nm_PrimerNombre VARCHAR(100) = NULL,
@IInt_Id_Pais BIGINT = NULL,
@Randon INT
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @CORREO_RETURN VARCHAR(100)
	DECLARE @IVch_nm_CorreoElectronico VARCHAR (100) = ''
	DECLARE @IVch_nm_CorreoElectronicoPrimeraParte VARCHAR(100)= ''
	DECLARE @Dominio VARCHAR (100) = ''
	DECLARE @IDSecuencial VARCHAR(10)=''

				SET @IVch_nm_CorreoElectronicoPrimeraParte = (SELECT replace(@IVch_nm_PrimerNombre, ' ', ''))+'.'+ (SELECT replace(@IVch_nm_PrimerApellido, ' ', ''));
				SET @Dominio = CASE WHEN (SELECT TOP 1 nm_Descripcion FROM Cidenet_Maestro.Paises WHERE Id_Paises= @IInt_Id_Pais)  LIKE '%colombia%' THEN '@cidenet.com.co' 
										ELSE '@cidenet.com.us'  END

				IF((SELECT COUNT(1) FROM Cidenet_Empleado.Empleados WHERE nm_CorreoElectronico LIKE '%'+@IVch_nm_CorreoElectronicoPrimeraParte+'%')>0)
					BEGIN
						SET @IDSecuencial = CAST(@Randon AS VARCHAR(10))
						SET @IVch_nm_CorreoElectronico = @IVch_nm_CorreoElectronicoPrimeraParte+@IDSecuencial+@Dominio
						IF((SELECT COUNT(1) FROM Cidenet_Empleado.Empleados WHERE nm_CorreoElectronico = @IVch_nm_CorreoElectronico)>0)
							BEGIN
								SET @IVch_nm_CorreoElectronico =''
								SET @Randon = @Randon - 1
								SET @IDSecuencial = CAST(@Randon AS VARCHAR(10))
								SET @IVch_nm_CorreoElectronico =@IVch_nm_CorreoElectronicoPrimeraParte+@IDSecuencial+@Dominio
							END
					END
				ELSE
					BEGIN
						SET @IVch_nm_CorreoElectronico = @IVch_nm_CorreoElectronicoPrimeraParte + @Dominio
					END


					SET @CORREO_RETURN = @IVch_nm_CorreoElectronico

RETURN @CORREO_RETURN
END




GO


