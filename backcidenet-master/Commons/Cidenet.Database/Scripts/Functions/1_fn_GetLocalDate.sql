USE [DataBaseCidenet]
GO

/****** Object:  UserDefinedFunction [Cidenet_Util].[fn_GetLocalDate]    Script Date: 19/08/2021 5:08:31 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Scheme   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripci�n: Creaci�n de Function
*************************************************************************************
** BIT�CORA:
*************************************************************************************
** Fecha:          Autor:					Descripci�n:
** 2021-08-16	   Edinson Paternina R		Creaci�n Inicial
*************************************************************************************/
CREATE FUNCTION [Cidenet_Util].[fn_GetLocalDate]
(
	
)
RETURNS DATETIME --RETORNO
AS
BEGIN   --INICIO DE FUNCION
	DECLARE @Date DATETIME = GETDATE()

	RETURN @Date
END
GO


