USE [DataBaseCidenet]
GO

/****** Object:  Table [Cidenet_Log].[Logs]    Script Date: 19/08/2021 5:07:03 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Cidenet_Log].[Logs](
	[Id_Log] [bigint] IDENTITY(1,1) NOT NULL,
	[nm_SToreProcedure] [varchar](100) NULL,
	[nm_LineaError] [varchar](100) NULL,
	[nm_DescripcionError] [varchar](max) NULL,
	[dt_Creado] [datetime2](7) NULL,
	[dt_Modificado] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Log] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [Cidenet_Log].[Logs] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Creado]
GO

ALTER TABLE [Cidenet_Log].[Logs] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Modificado]
GO


