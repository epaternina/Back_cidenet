USE [DataBaseCidenet]
GO

/****** Object:  Table [Cidenet_Maestro].[TipoDocumentos]    Script Date: 19/08/2021 5:05:18 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Cidenet_Maestro].[TipoDocumentos](
	[Id_TipoDocumentos] [bigint] IDENTITY(1,1) NOT NULL,
	[nm_Descripcion] [varchar](50) NOT NULL,
	[dt_Creado] [datetime2](7) NULL,
	[dt_Modificado] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_TipoDocumentos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [Cidenet_Maestro].[TipoDocumentos] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Creado]
GO

ALTER TABLE [Cidenet_Maestro].[TipoDocumentos] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Modificado]
GO


