USE [DataBaseCidenet]
GO

/****** Object:  Table [Cidenet_Empleado].[Empleados]    Script Date: 19/08/2021 5:07:33 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Cidenet_Empleado].[Empleados](
	[Id_Empleados] [bigint] IDENTITY(1,1) NOT NULL,
	[nm_PrimerApellido] [varchar](20) NOT NULL,
	[nm_SegundoApellido] [varchar](20) NOT NULL,
	[nm_PrimerNombre] [varchar](20) NOT NULL,
	[nm_OtrosNombres] [varchar](50) NULL,
	[Id_Pais] [bigint] NULL,
	[Id_TipoIdentificacion] [bigint] NULL,
	[cd_NumeroIdentificacion] [varchar](20) NOT NULL,
	[nm_CorreoElectronico] [varchar](300) NOT NULL,
	[dt_FechaIngreso] [datetime2](7) NOT NULL,
	[Id_Area] [bigint] NULL,
	[ind_Estado] [bit] NULL,
	[dt_FechaHoraRegistro] [datetime2](7) NOT NULL,
	[dt_Creado] [datetime2](7) NULL,
	[dt_Modificado] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Empleados] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [Cidenet_Empleado].[Empleados] ADD  DEFAULT ((1)) FOR [ind_Estado]
GO

ALTER TABLE [Cidenet_Empleado].[Empleados] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Creado]
GO

ALTER TABLE [Cidenet_Empleado].[Empleados] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Modificado]
GO

ALTER TABLE [Cidenet_Empleado].[Empleados]  WITH CHECK ADD FOREIGN KEY([Id_Area])
REFERENCES [Cidenet_Maestro].[Areas] ([Id_Areas])
GO

ALTER TABLE [Cidenet_Empleado].[Empleados]  WITH CHECK ADD FOREIGN KEY([Id_Pais])
REFERENCES [Cidenet_Maestro].[Paises] ([Id_Paises])
GO

ALTER TABLE [Cidenet_Empleado].[Empleados]  WITH CHECK ADD FOREIGN KEY([Id_TipoIdentificacion])
REFERENCES [Cidenet_Maestro].[TipoDocumentos] ([Id_TipoDocumentos])
GO


