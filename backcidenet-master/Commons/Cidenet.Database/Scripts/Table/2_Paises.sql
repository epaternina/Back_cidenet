USE [DataBaseCidenet]
GO

/****** Object:  Table [Cidenet_Maestro].[Paises]    Script Date: 19/08/2021 5:05:58 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Cidenet_Maestro].[Paises](
	[Id_Paises] [bigint] IDENTITY(1,1) NOT NULL,
	[nm_Descripcion] [varchar](50) NOT NULL,
	[dt_Creado] [datetime2](7) NULL,
	[dt_Modificado] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Paises] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [Cidenet_Maestro].[Paises] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Creado]
GO

ALTER TABLE [Cidenet_Maestro].[Paises] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Modificado]
GO


