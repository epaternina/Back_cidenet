USE [master]
GO
/****** Object:  Database [DataBaseCidenet]    Script Date: 19/08/2021 5:27:41 p. m. ******/
CREATE DATABASE [DataBaseCidenet]
 CONTAINMENT = NONE
GO
ALTER DATABASE [DataBaseCidenet] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DataBaseCidenet].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DataBaseCidenet] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET ARITHABORT OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DataBaseCidenet] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DataBaseCidenet] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DataBaseCidenet] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DataBaseCidenet] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DataBaseCidenet] SET  MULTI_USER 
GO
ALTER DATABASE [DataBaseCidenet] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DataBaseCidenet] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DataBaseCidenet] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DataBaseCidenet] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DataBaseCidenet] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [DataBaseCidenet] SET QUERY_STORE = OFF
GO
USE [DataBaseCidenet]
GO
/****** Object:  Schema [Cidenet_Empleado]    Script Date: 19/08/2021 5:27:41 p. m. ******/
CREATE SCHEMA [Cidenet_Empleado]
GO
/****** Object:  Schema [Cidenet_Log]    Script Date: 19/08/2021 5:27:41 p. m. ******/
CREATE SCHEMA [Cidenet_Log]
GO
/****** Object:  Schema [Cidenet_Maestro]    Script Date: 19/08/2021 5:27:41 p. m. ******/
CREATE SCHEMA [Cidenet_Maestro]
GO
/****** Object:  Schema [Cidenet_Util]    Script Date: 19/08/2021 5:27:41 p. m. ******/
CREATE SCHEMA [Cidenet_Util]
GO
/****** Object:  UserDefinedFunction [Cidenet_Empleado].[fn_Correo_Get]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripción: Creación de FN
*************************************************************************************
** BITÁCORA:
*************************************************************************************
** Fecha:          Autor:					Descripción:
** 2021-08-18	   Edinson Paternina R		Creación Inicial
*************************************************************************************/
CREATE FUNCTION [Cidenet_Empleado].[fn_Correo_Get]
(
@IVch_nm_PrimerApellido VARCHAR(100) = NULL,
@IVch_nm_PrimerNombre VARCHAR(100) = NULL,
@IInt_Id_Pais BIGINT = NULL,
@Randon INT
)

RETURNS VARCHAR(100)

AS
BEGIN
	DECLARE @CORREO_RETURN VARCHAR(100)
	DECLARE @IVch_nm_CorreoElectronico VARCHAR (100) = ''
	DECLARE @IVch_nm_CorreoElectronicoPrimeraParte VARCHAR(100)= ''
	DECLARE @Dominio VARCHAR (100) = ''
	DECLARE @IDSecuencial VARCHAR(10)=''

				SET @IVch_nm_CorreoElectronicoPrimeraParte = (SELECT replace(@IVch_nm_PrimerNombre, ' ', ''))+'.'+ (SELECT replace(@IVch_nm_PrimerApellido, ' ', ''));
				SET @Dominio = CASE WHEN (SELECT TOP 1 nm_Descripcion FROM Cidenet_Maestro.Paises WHERE Id_Paises= @IInt_Id_Pais)  LIKE '%colombia%' THEN '@cidenet.com.co' 
										ELSE '@cidenet.com.us'  END

				IF((SELECT COUNT(1) FROM Cidenet_Empleado.Empleados WHERE nm_CorreoElectronico LIKE '%'+@IVch_nm_CorreoElectronicoPrimeraParte+'%')>0)
					BEGIN
						SET @IDSecuencial = CAST(@Randon AS VARCHAR(10))
						SET @IVch_nm_CorreoElectronico = @IVch_nm_CorreoElectronicoPrimeraParte+@IDSecuencial+@Dominio
						IF((SELECT COUNT(1) FROM Cidenet_Empleado.Empleados WHERE nm_CorreoElectronico = @IVch_nm_CorreoElectronico)>0)
							BEGIN
								SET @IVch_nm_CorreoElectronico =''
								SET @Randon = @Randon - 1
								SET @IDSecuencial = CAST(@Randon AS VARCHAR(10))
								SET @IVch_nm_CorreoElectronico =@IVch_nm_CorreoElectronicoPrimeraParte+@IDSecuencial+@Dominio
							END
					END
				ELSE
					BEGIN
						SET @IVch_nm_CorreoElectronico = @IVch_nm_CorreoElectronicoPrimeraParte + @Dominio
					END


					SET @CORREO_RETURN = @IVch_nm_CorreoElectronico

RETURN @CORREO_RETURN
END




GO
/****** Object:  UserDefinedFunction [Cidenet_Util].[fn_GetLocalDate]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Scheme   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripción: Creación de Function
*************************************************************************************
** BITÁCORA:
*************************************************************************************
** Fecha:          Autor:					Descripción:
** 2021-08-16	   Edinson Paternina R		Creación Inicial
*************************************************************************************/
CREATE FUNCTION [Cidenet_Util].[fn_GetLocalDate]
(
	
)
RETURNS DATETIME --RETORNO
AS
BEGIN   --INICIO DE FUNCION
	DECLARE @Date DATETIME = GETDATE()

	RETURN @Date
END
GO
/****** Object:  Table [Cidenet_Empleado].[Empleados]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Cidenet_Empleado].[Empleados](
	[Id_Empleados] [bigint] IDENTITY(1,1) NOT NULL,
	[nm_PrimerApellido] [varchar](20) NOT NULL,
	[nm_SegundoApellido] [varchar](20) NOT NULL,
	[nm_PrimerNombre] [varchar](20) NOT NULL,
	[nm_OtrosNombres] [varchar](50) NULL,
	[Id_Pais] [bigint] NULL,
	[Id_TipoIdentificacion] [bigint] NULL,
	[cd_NumeroIdentificacion] [varchar](20) NOT NULL,
	[nm_CorreoElectronico] [varchar](300) NOT NULL,
	[dt_FechaIngreso] [datetime2](7) NOT NULL,
	[Id_Area] [bigint] NULL,
	[ind_Estado] [bit] NULL,
	[dt_FechaHoraRegistro] [datetime2](7) NOT NULL,
	[dt_Creado] [datetime2](7) NULL,
	[dt_Modificado] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Empleados] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Cidenet_Log].[Logs]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Cidenet_Log].[Logs](
	[Id_Log] [bigint] IDENTITY(1,1) NOT NULL,
	[nm_SToreProcedure] [varchar](100) NULL,
	[nm_LineaError] [varchar](100) NULL,
	[nm_DescripcionError] [varchar](max) NULL,
	[dt_Creado] [datetime2](7) NULL,
	[dt_Modificado] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Log] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Cidenet_Maestro].[Areas]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Cidenet_Maestro].[Areas](
	[Id_Areas] [bigint] IDENTITY(1,1) NOT NULL,
	[nm_Descripcion] [varchar](50) NOT NULL,
	[dt_Creado] [datetime2](7) NULL,
	[dt_Modificado] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Areas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Cidenet_Maestro].[Paises]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Cidenet_Maestro].[Paises](
	[Id_Paises] [bigint] IDENTITY(1,1) NOT NULL,
	[nm_Descripcion] [varchar](50) NOT NULL,
	[dt_Creado] [datetime2](7) NULL,
	[dt_Modificado] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Paises] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Cidenet_Maestro].[TipoDocumentos]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Cidenet_Maestro].[TipoDocumentos](
	[Id_TipoDocumentos] [bigint] IDENTITY(1,1) NOT NULL,
	[nm_Descripcion] [varchar](50) NOT NULL,
	[dt_Creado] [datetime2](7) NULL,
	[dt_Modificado] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_TipoDocumentos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [Cidenet_Empleado].[Empleados] ON 
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (7, N'Corrales', N'Hernandez', N'Anita2', N'Sofia', 1, 2, N'447832552', N'Anita2.Corrales@cidenet.com.co', CAST(N'2021-08-10T00:00:00.0000000' AS DateTime2), 1, 1, CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-19T01:02:12.6066667' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (8, N'Zapata', N'Trespalacios', N'Gabriel', N'Maria', 2, 3, N'11433746311', N'Zapata@hotmail.com', CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), 3, 1, CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (9, N'Lguizamo', N'suescun', N'Jhon', N'antonio', 1, 4, N'11433746316', N'Lguizamo@hotmail.com', CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), 4, 1, CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (10, N'perez', N'Marlon', N'Jose', N'rodolfo', 2, 1, N'11433746371', N'Marlon@hotmail.com', CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), 5, 1, CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (11, N'Torres', N'Fray', N'jj', N'Maria', 1, 2, N'11433746311', N'Fray@hotmail.com', CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), 1, 1, CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (12, N'Lopez', N'Muñoz', N'Kelly', N'Maria', 2, 3, N'11433746511', N'Lopez@hotmail.com', CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), 2, 1, CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (13, N'Ortiz', N'Osorio', N'Karina', N'Sofia', 1, 4, N'11433786311', N'Ortiz@hotmail.com', CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), 3, 1, CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (14, N'Osorio', N'Olivera', N'Angelica', N'Maria', 2, 1, N'11433476311', N'Osorio@hotmail.com', CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), 4, 1, CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (15, N'Luz', N'Padilla', N'Diaz', N'Mari', 1, 2, N'11433246311', N'Luz@hotmail.com', CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), 5, 1, CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (16, N'Cantillo', N'Palacios', N'Amelys', N'Jhoana', 2, 3, N'1143456311', N'Cantillo@hotmail.com', CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), 1, 1, CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2), CAST(N'2021-08-17T00:00:00.0000000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (18, N'Buen dia', N'Hernandez', N'Camila', N'Sofia', 1, 2, N'44783252', N'Camila.Buendia@cidenet.com.co', CAST(N'2021-08-10T00:00:00.0000000' AS DateTime2), 1, 1, CAST(N'2021-08-18T22:13:51.5433333' AS DateTime2), CAST(N'2021-08-18T22:13:51.5433333' AS DateTime2), CAST(N'2021-08-18T22:13:51.5433333' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (22, N'Paternina', N'Rivera', N'Edinson', N'Jesús', 1, 1, N'1143374631', N'Edinson.Paternina92@cidenet.com.co', CAST(N'2018-08-01T17:16:40.0000000' AS DateTime2), 3, 1, CAST(N'2021-08-19T10:34:01.2400000' AS DateTime2), CAST(N'2021-08-19T10:34:01.2400000' AS DateTime2), CAST(N'2021-08-19T10:34:01.2400000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (23, N'Ramos', N'Gonzalez', N'Carolina', N'Isabell', 2, 2, N'6464564645', N'Carolina.Ramos@cidenet.com.us', CAST(N'2021-08-19T05:00:00.0000000' AS DateTime2), 5, 1, CAST(N'2021-08-19T13:16:45.2833333' AS DateTime2), CAST(N'2021-08-19T13:16:45.2833333' AS DateTime2), CAST(N'2021-08-19T13:16:45.2833333' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (24, N'Castellar ', N'Abello ', N'Julio', N'Andres', 1, 1, N'114576535', N'Julio.Castellar@cidenet.com.co', CAST(N'2021-08-19T05:00:00.0000000' AS DateTime2), 3, 1, CAST(N'2021-08-19T13:23:39.4900000' AS DateTime2), CAST(N'2021-08-19T13:23:39.4900000' AS DateTime2), CAST(N'2021-08-19T13:23:39.4900000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (25, N'Castellar ', N'Abello ', N'Julio', N'Andres', 1, 1, N'1145765535', N'Julio.Castellar85@cidenet.com.co', CAST(N'2021-08-19T05:00:00.0000000' AS DateTime2), 3, 1, CAST(N'2021-08-19T13:24:49.6966667' AS DateTime2), CAST(N'2021-08-19T13:24:49.6966667' AS DateTime2), CAST(N'2021-08-19T13:24:49.6966667' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (26, N'Lopez', N'perez', N'alejandra', N'Manuela', 2, 1, N'5454656', N'alejandra.Lopez@cidenet.com.us', CAST(N'2021-08-19T05:00:00.0000000' AS DateTime2), 2, 1, CAST(N'2021-08-19T13:27:09.5866667' AS DateTime2), CAST(N'2021-08-19T13:27:09.5866667' AS DateTime2), CAST(N'2021-08-19T13:27:09.5866667' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (27, N'Olivarez', N'Rivera', N'Jorge', N'Armando', 1, 1, N'1145874321316', N'Jorge.Olivarez@cidenet.com.co', CAST(N'2021-08-19T05:00:00.0000000' AS DateTime2), 2, 1, CAST(N'2021-08-19T13:29:09.7900000' AS DateTime2), CAST(N'2021-08-19T13:29:09.7900000' AS DateTime2), CAST(N'2021-08-19T13:29:09.7900000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (28, N'Marriaga', N'soza', N'Carlos', N'Danilo', 2, 2, N'326262', N'Carlos.Marriaga@cidenet.com.us', CAST(N'2021-08-19T05:00:00.0000000' AS DateTime2), 1, 1, CAST(N'2021-08-19T13:36:13.3966667' AS DateTime2), CAST(N'2021-08-19T13:36:13.3966667' AS DateTime2), CAST(N'2021-08-19T13:36:13.3966667' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (30, N'Paternina', N'Rivera', N'Yecenia', N'Maria', 2, 2, N'323232323', N'Yecenia.Paternina@cidenet.com.us', CAST(N'2021-08-19T05:00:00.0000000' AS DateTime2), 2, 1, CAST(N'2021-08-19T13:40:37.3866667' AS DateTime2), CAST(N'2021-08-19T13:40:37.3866667' AS DateTime2), CAST(N'2021-08-19T13:40:37.3866667' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (31, N'Segrerap', N'Lopezk', N'Manuel', N'camilo', 2, 2, N'2121223132', N'Manuel.Segrerap@cidenet.com.us', CAST(N'2021-08-19T05:00:00.0000000' AS DateTime2), 3, 1, CAST(N'2021-08-19T13:41:34.7233333' AS DateTime2), CAST(N'2021-08-19T13:41:34.7233333' AS DateTime2), CAST(N'2021-08-19T16:04:35.2266667' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (34, N'Trespalacios', N'Martinez', N'Daniel', N'Andres', 1, 2, N'4454545465', N'Daniel.Trespalacios@cidenet.com.co', CAST(N'2021-08-19T05:00:00.0000000' AS DateTime2), 2, 1, CAST(N'2021-08-19T14:01:27.8166667' AS DateTime2), CAST(N'2021-08-19T14:01:27.8166667' AS DateTime2), CAST(N'2021-08-19T14:01:27.8166667' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (35, N'Martinez', N'Suan', N'Lian', N'', 2, 1, N'54545', N'Lian.Martinez@cidenet.com.us', CAST(N'2021-08-19T05:00:00.0000000' AS DateTime2), 2, 1, CAST(N'2021-08-19T14:02:44.9233333' AS DateTime2), CAST(N'2021-08-19T14:02:44.9233333' AS DateTime2), CAST(N'2021-08-19T14:02:44.9233333' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (37, N'Gonzalez', N'martinez', N'Minerba', N'rosa', 2, 2, N'4656432356', N'Minerba.Gonzalez@cidenet.com.us', CAST(N'2021-08-04T05:00:00.0000000' AS DateTime2), 3, 1, CAST(N'2021-08-19T16:53:11.0900000' AS DateTime2), CAST(N'2021-08-19T16:53:11.0900000' AS DateTime2), CAST(N'2021-08-19T16:53:11.0900000' AS DateTime2))
GO
INSERT [Cidenet_Empleado].[Empleados] ([Id_Empleados], [nm_PrimerApellido], [nm_SegundoApellido], [nm_PrimerNombre], [nm_OtrosNombres], [Id_Pais], [Id_TipoIdentificacion], [cd_NumeroIdentificacion], [nm_CorreoElectronico], [dt_FechaIngreso], [Id_Area], [ind_Estado], [dt_FechaHoraRegistro], [dt_Creado], [dt_Modificado]) VALUES (45, N'Perez', N'alctraz', N'Mirian', N'jose', 2, 1, N'87636564', N'Mirian.Perez@cidenet.com.us', CAST(N'2021-08-18T05:00:00.0000000' AS DateTime2), 2, 1, CAST(N'2021-08-19T16:56:24.6733333' AS DateTime2), CAST(N'2021-08-19T16:56:24.6733333' AS DateTime2), CAST(N'2021-08-19T16:56:24.6733333' AS DateTime2))
GO
SET IDENTITY_INSERT [Cidenet_Empleado].[Empleados] OFF
GO
SET IDENTITY_INSERT [Cidenet_Log].[Logs] ON 
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (1, N'Cidenet_Empleado.pr_Empleados_Create', N'31', N'Divide by zero error encountered.', CAST(N'2021-08-18T22:09:05.2466667' AS DateTime2), CAST(N'2021-08-18T22:09:05.2466667' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (2, N'Cidenet_Empleado.pr_Empleados_Create', N'31', N'Divide by zero error encountered.', CAST(N'2021-08-18T22:09:55.8266667' AS DateTime2), CAST(N'2021-08-18T22:09:55.8266667' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (3, N'Cidenet_Empleado.pr_Empleados_Create', N'31', N'Divide by zero error encountered.', CAST(N'2021-08-18T22:12:51.0466667' AS DateTime2), CAST(N'2021-08-18T22:12:51.0466667' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (4, N'Cidenet_Empleado.pr_Empleados_Create', N'35', N'The INSERT statement conflicted with the FOREIGN KEY constraint "FK__Empleados__Id_Ar__70DDC3D8". The conflict occurred in database "DataBaseCidenet", table "Cidenet_Maestro.Areas", column ''Id_Areas''.', CAST(N'2021-08-18T22:14:25.0166667' AS DateTime2), CAST(N'2021-08-18T22:14:25.0166667' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (5, N'Cidenet_Empleado.pr_Empleados_Create', N'35', N'The INSERT statement conflicted with the FOREIGN KEY constraint "FK__Empleados__Id_Ar__70DDC3D8". The conflict occurred in database "DataBaseCidenet", table "Cidenet_Maestro.Areas", column ''Id_Areas''.', CAST(N'2021-08-18T22:52:01.4066667' AS DateTime2), CAST(N'2021-08-18T22:52:01.4066667' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (6, N'Cidenet_Empleado.pr_Empleados_UPDATE', N'44', N'The UPDATE statement conflicted with the FOREIGN KEY constraint "FK__Empleados__Id_Ar__70DDC3D8". The conflict occurred in database "DataBaseCidenet", table "Cidenet_Maestro.Areas", column ''Id_Areas''.', CAST(N'2021-08-19T00:57:35.0766667' AS DateTime2), CAST(N'2021-08-19T00:57:35.0766667' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (7, N'Cidenet_Empleado.pr_Empleados_Create', N'35', N'Cannot insert the value NULL into column ''nm_PrimerApellido'', table ''DataBaseCidenet.Cidenet_Empleado.Empleados''; column does not allow nulls. INSERT fails.', CAST(N'2021-08-19T16:53:32.0000000' AS DateTime2), CAST(N'2021-08-19T16:53:32.0000000' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (8, N'Cidenet_Empleado.pr_Empleados_Create', N'35', N'Cannot insert the value NULL into column ''nm_PrimerApellido'', table ''DataBaseCidenet.Cidenet_Empleado.Empleados''; column does not allow nulls. INSERT fails.', CAST(N'2021-08-19T16:53:35.2100000' AS DateTime2), CAST(N'2021-08-19T16:53:35.2100000' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (9, N'Cidenet_Empleado.pr_Empleados_Create', N'35', N'Cannot insert the value NULL into column ''nm_PrimerApellido'', table ''DataBaseCidenet.Cidenet_Empleado.Empleados''; column does not allow nulls. INSERT fails.', CAST(N'2021-08-19T16:53:35.8833333' AS DateTime2), CAST(N'2021-08-19T16:53:35.8833333' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (10, N'Cidenet_Empleado.pr_Empleados_Create', N'35', N'Cannot insert the value NULL into column ''nm_PrimerApellido'', table ''DataBaseCidenet.Cidenet_Empleado.Empleados''; column does not allow nulls. INSERT fails.', CAST(N'2021-08-19T16:53:36.4833333' AS DateTime2), CAST(N'2021-08-19T16:53:36.4833333' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (11, N'Cidenet_Empleado.pr_Empleados_Create', N'35', N'Cannot insert the value NULL into column ''nm_PrimerApellido'', table ''DataBaseCidenet.Cidenet_Empleado.Empleados''; column does not allow nulls. INSERT fails.', CAST(N'2021-08-19T16:53:39.2200000' AS DateTime2), CAST(N'2021-08-19T16:53:39.2200000' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (12, N'Cidenet_Empleado.pr_Empleados_Create', N'35', N'Cannot insert the value NULL into column ''nm_PrimerApellido'', table ''DataBaseCidenet.Cidenet_Empleado.Empleados''; column does not allow nulls. INSERT fails.', CAST(N'2021-08-19T16:53:44.8700000' AS DateTime2), CAST(N'2021-08-19T16:53:44.8700000' AS DateTime2))
GO
INSERT [Cidenet_Log].[Logs] ([Id_Log], [nm_SToreProcedure], [nm_LineaError], [nm_DescripcionError], [dt_Creado], [dt_Modificado]) VALUES (13, N'Cidenet_Empleado.pr_Empleados_Create', N'35', N'Cannot insert the value NULL into column ''nm_SegundoApellido'', table ''DataBaseCidenet.Cidenet_Empleado.Empleados''; column does not allow nulls. INSERT fails.', CAST(N'2021-08-19T16:53:56.4300000' AS DateTime2), CAST(N'2021-08-19T16:53:56.4300000' AS DateTime2))
GO
SET IDENTITY_INSERT [Cidenet_Log].[Logs] OFF
GO
SET IDENTITY_INSERT [Cidenet_Maestro].[Areas] ON 
GO
INSERT [Cidenet_Maestro].[Areas] ([Id_Areas], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (1, N'Administración', CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2), CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2))
GO
INSERT [Cidenet_Maestro].[Areas] ([Id_Areas], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (2, N'Financiera', CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2), CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2))
GO
INSERT [Cidenet_Maestro].[Areas] ([Id_Areas], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (3, N'Compras', CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2), CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2))
GO
INSERT [Cidenet_Maestro].[Areas] ([Id_Areas], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (4, N'Infraestructura', CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2), CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2))
GO
INSERT [Cidenet_Maestro].[Areas] ([Id_Areas], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (5, N'Operación', CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2), CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2))
GO
INSERT [Cidenet_Maestro].[Areas] ([Id_Areas], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (6, N'Talento Humano', CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2), CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2))
GO
INSERT [Cidenet_Maestro].[Areas] ([Id_Areas], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (7, N'Servicios Varios', CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2), CAST(N'2021-08-17T14:08:27.7300000' AS DateTime2))
GO
SET IDENTITY_INSERT [Cidenet_Maestro].[Areas] OFF
GO
SET IDENTITY_INSERT [Cidenet_Maestro].[Paises] ON 
GO
INSERT [Cidenet_Maestro].[Paises] ([Id_Paises], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (1, N'Colombia', CAST(N'2021-08-17T14:00:12.8066667' AS DateTime2), CAST(N'2021-08-17T14:00:12.8166667' AS DateTime2))
GO
INSERT [Cidenet_Maestro].[Paises] ([Id_Paises], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (2, N'Estados Unidos', CAST(N'2021-08-17T14:00:12.8166667' AS DateTime2), CAST(N'2021-08-17T14:00:12.8166667' AS DateTime2))
GO
SET IDENTITY_INSERT [Cidenet_Maestro].[Paises] OFF
GO
SET IDENTITY_INSERT [Cidenet_Maestro].[TipoDocumentos] ON 
GO
INSERT [Cidenet_Maestro].[TipoDocumentos] ([Id_TipoDocumentos], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (1, N'Cédula de Ciudadania', CAST(N'2021-08-17T14:02:21.3300000' AS DateTime2), CAST(N'2021-08-17T14:02:21.3300000' AS DateTime2))
GO
INSERT [Cidenet_Maestro].[TipoDocumentos] ([Id_TipoDocumentos], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (2, N'Cédula Extranjería', CAST(N'2021-08-17T14:02:21.3300000' AS DateTime2), CAST(N'2021-08-17T14:02:21.3300000' AS DateTime2))
GO
INSERT [Cidenet_Maestro].[TipoDocumentos] ([Id_TipoDocumentos], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (3, N'Pasaporte', CAST(N'2021-08-17T14:02:21.3300000' AS DateTime2), CAST(N'2021-08-17T14:02:21.3300000' AS DateTime2))
GO
INSERT [Cidenet_Maestro].[TipoDocumentos] ([Id_TipoDocumentos], [nm_Descripcion], [dt_Creado], [dt_Modificado]) VALUES (4, N'Permiso Especial', CAST(N'2021-08-17T14:02:21.3300000' AS DateTime2), CAST(N'2021-08-17T14:02:21.3300000' AS DateTime2))
GO
SET IDENTITY_INSERT [Cidenet_Maestro].[TipoDocumentos] OFF
GO
ALTER TABLE [Cidenet_Empleado].[Empleados] ADD  DEFAULT ((1)) FOR [ind_Estado]
GO
ALTER TABLE [Cidenet_Empleado].[Empleados] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Creado]
GO
ALTER TABLE [Cidenet_Empleado].[Empleados] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Modificado]
GO
ALTER TABLE [Cidenet_Log].[Logs] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Creado]
GO
ALTER TABLE [Cidenet_Log].[Logs] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Modificado]
GO
ALTER TABLE [Cidenet_Maestro].[Areas] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Creado]
GO
ALTER TABLE [Cidenet_Maestro].[Areas] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Modificado]
GO
ALTER TABLE [Cidenet_Maestro].[Paises] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Creado]
GO
ALTER TABLE [Cidenet_Maestro].[Paises] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Modificado]
GO
ALTER TABLE [Cidenet_Maestro].[TipoDocumentos] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Creado]
GO
ALTER TABLE [Cidenet_Maestro].[TipoDocumentos] ADD  DEFAULT ([Cidenet_Util].[fn_GetLocalDate]()) FOR [dt_Modificado]
GO
ALTER TABLE [Cidenet_Empleado].[Empleados]  WITH CHECK ADD FOREIGN KEY([Id_Area])
REFERENCES [Cidenet_Maestro].[Areas] ([Id_Areas])
GO
ALTER TABLE [Cidenet_Empleado].[Empleados]  WITH CHECK ADD FOREIGN KEY([Id_Pais])
REFERENCES [Cidenet_Maestro].[Paises] ([Id_Paises])
GO
ALTER TABLE [Cidenet_Empleado].[Empleados]  WITH CHECK ADD FOREIGN KEY([Id_TipoIdentificacion])
REFERENCES [Cidenet_Maestro].[TipoDocumentos] ([Id_TipoDocumentos])
GO
/****** Object:  StoredProcedure [Cidenet_Empleado].[pr_Empleados_Create]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripción: Creación de SP
*************************************************************************************
** BITÁCORA:
*************************************************************************************
** Fecha:          Autor:					Descripción:
** 2021-08-18	   Edinson Paternina R		Creación Inicial
*************************************************************************************/
CREATE PROCEDURE [Cidenet_Empleado].[pr_Empleados_Create]
@IVch_nm_PrimerApellido VARCHAR(100) = NULL,
@IVch_nm_SegundoApellido VARCHAR(100) = NULL,
@IVch_nm_PrimerNombre VARCHAR(100) = NULL,
@IVch_nm_OtrosNombres VARCHAR(100) = NULL,
@IInt_Id_Pais BIGINT = NULL,
@IIntId_TipoIdentificacion BIGINT = NULL,
@IVch_cd_NumeroIdentificacion VARCHAR(100) = NULL,
@IDt_FechaIngreso DATETIME = NULL,
@IInt_Id_Area BIGINT = NULL
AS

BEGIN

	DECLARE @IVch_nm_CorreoElectronico VARCHAR (100) = ''
	DECLARE @IVch_Mensaje VARCHAR (100) = 'Registro Exitoso!'
	

	BEGIN TRY 

		IF((SELECT COUNT(1)  FROM Cidenet_Empleado.Empleados WHERE cd_NumeroIdentificacion = @IVch_cd_NumeroIdentificacion)=0 )
			BEGIN
				
				SET @IVch_nm_CorreoElectronico = (SELECT Cidenet_Empleado.fn_Correo_Get(@IVch_nm_PrimerApellido, @IVch_nm_PrimerNombre,@IInt_Id_Pais,ROUND(((99 - 2) * RAND() + 1), 0)))

						INSERT INTO Cidenet_Empleado.Empleados (nm_PrimerApellido,nm_SegundoApellido, nm_PrimerNombre, nm_OtrosNombres,
																Id_Pais, Id_TipoIdentificacion, cd_NumeroIdentificacion, nm_CorreoElectronico,
																dt_FechaIngreso, Id_Area, ind_Estado, dt_FechaHoraRegistro, dt_Creado, dt_Modificado)
														 VALUES(@IVch_nm_PrimerApellido, @IVch_nm_SegundoApellido, @IVch_nm_PrimerNombre, @IVch_nm_OtrosNombres,
																@IInt_Id_Pais, @IIntId_TipoIdentificacion, @IVch_cd_NumeroIdentificacion, @IVch_nm_CorreoElectronico,
																@IDt_FechaIngreso,@IInt_Id_Area,1, Cidenet_Util.fn_GetLocalDate(), Cidenet_Util.fn_GetLocalDate(), Cidenet_Util.fn_GetLocalDate())
			 END
		
		ELSE
			 BEGIN
				 SET @IVch_Mensaje = 'Ya Existe este empleado!'
			 END

			SELECT @IVch_Mensaje AS Result

	END TRY
	BEGIN CATCH
	INSERT Cidenet_Log.Logs (nm_SToreProcedure, nm_LineaError, nm_DescripcionError, dt_Creado, dt_Modificado)
	SELECT ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE(),Cidenet_Util.fn_GetLocalDate(),Cidenet_Util.fn_GetLocalDate()

	SET  @IVch_Mensaje = 'Ocurrio un error al insertar el Empleado, revisar Logs!'
	SELECT @IVch_Mensaje AS Result

	END CATCH

END

GO
/****** Object:  StoredProcedure [Cidenet_Empleado].[pr_Empleados_Delete]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripción: Creación de SP
*************************************************************************************
** BITÁCORA:
*************************************************************************************
** Fecha:          Autor:					Descripción:
** 2021-08-18	   Edinson Paternina R		Creación Inicial
*************************************************************************************/

CREATE PROCEDURE [Cidenet_Empleado].[pr_Empleados_Delete]
@cd_NumeroIdentificacion VARCHAR(100) = NULL
AS

BEGIN

	DECLARE @IVch_Mensaje VARCHAR (100) = 'Empleado eliminado de forma Exitosa!'

	BEGIN TRY 

		IF((SELECT COUNT(1)  FROM Cidenet_Empleado.Empleados WHERE cd_NumeroIdentificacion = @cd_NumeroIdentificacion)>0)
			BEGIN
				DELETE FROM Cidenet_Empleado.Empleados WHERE cd_NumeroIdentificacion = @cd_NumeroIdentificacion
			END
		ELSE
			BEGIN
				SET @IVch_Mensaje = 'No Existe este empleado!'
			END

			SELECT @IVch_Mensaje AS Result

	END TRY
	BEGIN CATCH
	INSERT Cidenet_Log.Logs (nm_SToreProcedure, nm_LineaError, nm_DescripcionError, dt_Creado, dt_Modificado)
	SELECT ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE(),Cidenet_Util.fn_GetLocalDate(),Cidenet_Util.fn_GetLocalDate()

	SET  @IVch_Mensaje = 'Ocurrio un error al insertar el Empleado, revisar Logs!'
	SELECT @IVch_Mensaje AS Result

	END CATCH

END
GO
/****** Object:  StoredProcedure [Cidenet_Empleado].[pr_Empleados_Get]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripción: Creación de SP
*************************************************************************************
** BITÁCORA:
*************************************************************************************
** Fecha:          Autor:					Descripción:
** 2021-08-16	   Edinson Paternina R		Creación Inicial
*************************************************************************************/
CREATE PROCEDURE [Cidenet_Empleado].[pr_Empleados_Get]
@IVch_nm_PrimerApellido VARCHAR(100) = NULL,
@IVch_nm_SegundoApellido VARCHAR(100) = NULL,
@IVch_nm_PrimerNombre VARCHAR(100) = NULL,
@IVch_nm_OtrosNombres VARCHAR(100) = NULL,
@IIntId_TipoIdentificacion BIGINT = NULL,
@IVch_cd_NumeroIdentificacion VARCHAR(100) = NULL,
@IInt_Id_Pais BIGINT = NULL,
@IVch_nm_CorreoElectronico VARCHAR(100) = NULL,
@Ib_ind_Estado BIT = NULL,
--Paginación
@PageNumber	INT = 1,
@PageSize	INT = 10

AS

BEGIN

--Sección de Declaraciones
DECLARE @vVch_Consulta NVARCHAR(MAX) = ''
DECLARE @vVch_WHERE NVARCHAR(MAX) = '  WHERE 1=1' 
DECLARE @vVch_Prametros NVARCHAR(MAX) = ''
DECLARE @vVch_ORDERBY NVARCHAR(MAX) = ''
DECLARE @vVch_FROM NVARCHAR(MAX) = ''

BEGIN TRY 

	IF(@IVch_nm_PrimerApellido IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.nm_PrimerApellido = @IVch_nm_PrimerApellido)'
		END

	IF(@IVch_nm_SegundoApellido IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.nm_SegundoApellido = @IVch_nm_SegundoApellido)'
		END

	IF(@IVch_nm_OtrosNombres IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.nm_OtrosNombres = @IVch_nm_OtrosNombres)'
		END

	IF(@IVch_nm_PrimerNombre IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.nm_PrimerNombre = @IVch_nm_PrimerNombre)'
		END

	IF(@IIntId_TipoIdentificacion IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.Id_TipoIdentificacion = @IIntId_TipoIdentificacion)'
		END

	IF(@IVch_cd_NumeroIdentificacion IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.cd_NumeroIdentificacion = @IVch_cd_NumeroIdentificacion)'
		END

	IF(@IInt_Id_Pais IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.Id_Pais = @IInt_Id_Pais)'
		END

	IF(@IVch_nm_CorreoElectronico IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.nm_CorreoElectronico = @IVch_nm_CorreoElectronico)'
		END

	IF(@Ib_ind_Estado IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.ind_Estado = @Ib_ind_Estado)'
		END
	
	IF(@IVch_nm_PrimerApellido IS NOT NULL OR @IVch_nm_SegundoApellido IS NOT NULL OR @IVch_nm_PrimerNombre IS NOT NULL OR 
	   @IVch_nm_OtrosNombres IS NOT NULL OR  @IIntId_TipoIdentificacion IS NOT NULL OR @IVch_cd_NumeroIdentificacion IS NOT NULL OR 
	   @IInt_Id_Pais IS NOT NULL OR @IVch_nm_CorreoElectronico IS NOT NULL OR @Ib_ind_Estado IS NOT NULL)
		BEGIN
			SET @PageNumber	 = 1
			SET @PageSize	 = 10
		END
		
	 SET  @vVch_Prametros = N'
							@IVch_nm_PrimerApellido VARCHAR(100) = NULL,
							@IVch_nm_SegundoApellido VARCHAR(100) = NULL,
							@IVch_nm_OtrosNombres VARCHAR(100) = NULL,
							@IVch_nm_PrimerNombre VARCHAR(100) = NULL,
							@IIntId_TipoIdentificacion BIGINT = NULL,
							@IVch_cd_NumeroIdentificacion VARCHAR(100) = NULL,
							@IInt_Id_Pais BIGINT = NULL,
							@IVch_nm_CorreoElectronico VARCHAR(100) = NULL,
							@Ib_ind_Estado BIT = NULL,
							@PageNumber INT = 1,
							@PageSize INT = 10'

	 SET @vVch_ORDERBY	  = 'ORDER BY  E.dt_Creado DESC
							 OFFSET (@PageNumber-1)*ISNULL(@PageSize,1) ROWS FETCH NEXT ISNULL(@PageSize,@@ROWCOUNT) ROWS ONLY'
	
	 SET @vVch_FROM		  = 'FROM Cidenet_Empleado.Empleados AS E 
							 INNER JOIN Cidenet_Maestro.Paises P  ON P.Id_Paises = E.Id_Pais
							 INNER JOIN Cidenet_Maestro.TipoDocumentos TD  ON TD.Id_TipoDocumentos = E.Id_TipoIdentificacion
							 INNER JOIN Cidenet_Maestro.Areas A  ON A.Id_Areas = E.Id_Area'

	 SET @vVch_Consulta	  ='SELECT 
							  E.Id_Empleados
							 ,E.nm_PrimerApellido
							 ,E.nm_SegundoApellido
							 ,E.nm_PrimerNombre
							 ,E.nm_OtrosNombres
							 ,P.nm_Descripcion AS nm_Pais
							 ,TD.nm_Descripcion AS cd_TipoIdentificacion
							 ,E.cd_NumeroIdentificacion
							 ,E.nm_CorreoElectronico
							 ,E.dt_FechaIngreso
							 ,A.nm_Descripcion AS nm_Area
							 ,E.ind_Estado
							 ,E.dt_FechaHoraRegistro
							 ,E.dt_Creado
							 ,TotalRecords = COUNT(1) OVER()' 
							 + @vVch_FROM +  @vVch_WHERE +  @vVch_ORDERBY

 EXECUTE sp_executesql
             @statement						= @vVch_Consulta
			,@params						= @vVch_Prametros
			,@IVch_nm_PrimerApellido		= @IVch_nm_PrimerApellido
			,@IVch_nm_SegundoApellido		= @IVch_nm_SegundoApellido
			,@IVch_nm_OtrosNombres			= @IVch_nm_OtrosNombres
			,@IVch_nm_PrimerNombre			= @IVch_nm_PrimerNombre
			,@IIntId_TipoIdentificacion		= @IIntId_TipoIdentificacion
			,@IVch_cd_NumeroIdentificacion	= @IVch_cd_NumeroIdentificacion
			,@IInt_Id_Pais					= @IInt_Id_Pais
			,@IVch_nm_CorreoElectronico		= @IVch_nm_CorreoElectronico
			,@Ib_ind_Estado					= @Ib_ind_Estado
			,@PageNumber 					= @PageNumber 
			,@PageSize						= @PageSize
END TRY
	BEGIN CATCH
		INSERT Cidenet_Log.Logs (nm_SToreProcedure, nm_LineaError, nm_DescripcionError, dt_Creado, dt_Modificado)
		SELECT ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE(),Cidenet_Util.fn_GetLocalDate(),Cidenet_Util.fn_GetLocalDate()


	END CATCH
	
END 

GO
/****** Object:  StoredProcedure [Cidenet_Empleado].[pr_Empleados_Update]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripción: Creación de SP
*************************************************************************************
** BITÁCORA:
*************************************************************************************
** Fecha:          Autor:					Descripción:
** 2021-08-18	   Edinson Paternina R		Creación Inicial
*************************************************************************************/


CREATE PROCEDURE [Cidenet_Empleado].[pr_Empleados_Update]
@IInt_Id_Empleados BIGINT = NULL,
@IVch_nm_PrimerApellido VARCHAR(100) = NULL,
@IVch_nm_SegundoApellido VARCHAR(100) = NULL,
@IVch_nm_PrimerNombre VARCHAR(100) = NULL,
@IVch_nm_OtrosNombres VARCHAR(100) = NULL,
@IInt_Id_Pais BIGINT = NULL,
@IIntId_TipoIdentificacion BIGINT = NULL,
@IVch_cd_NumeroIdentificacion VARCHAR(100) = NULL,
@IDt_FechaIngreso DATETIME = NULL,
@IInt_Id_Area BIGINT = NULL
AS

BEGIN

	DECLARE @IVch_nm_CorreoElectronico VARCHAR (100) = ''
	DECLARE @IVch_Mensaje VARCHAR (100) = 'Actualización Exitosa!'
	DECLARE @PrimerApellido VARCHAR (100) = ''
	DECLARE @PrimerNombre VARCHAR (100) = ''
	

	BEGIN TRY 

		IF((SELECT COUNT(1)  FROM Cidenet_Empleado.Empleados WHERE Id_Empleados = @IInt_Id_Empleados)>0 )
			BEGIN
				
				SET @PrimerApellido = ( SELECT TOP 1 nm_PrimerApellido FROM Cidenet_Empleado.Empleados WHERE Id_Empleados = @IInt_Id_Empleados)
				SET @PrimerNombre =	  ( SELECT TOP 1 nm_PrimerNombre FROM Cidenet_Empleado.Empleados WHERE Id_Empleados = @IInt_Id_Empleados)
				SET @IVch_nm_CorreoElectronico = (SELECT TOP 1 nm_CorreoElectronico FROM Cidenet_Empleado.Empleados WHERE Id_Empleados = @IInt_Id_Empleados)

				IF(@PrimerApellido!=@IVch_nm_PrimerApellido OR @PrimerNombre != @IVch_nm_PrimerNombre)
					BEGIN
						SET @IVch_nm_CorreoElectronico = (SELECT Cidenet_Empleado.fn_Correo_Get(@IVch_nm_PrimerApellido, @IVch_nm_PrimerNombre,@IInt_Id_Pais,ROUND(((99 - 2) * RAND() + 1), 0)))
					END

				UPDATE Cidenet_Empleado.Empleados
				SET nm_PrimerApellido= @IVch_nm_PrimerApellido,
					nm_SegundoApellido = @IVch_nm_SegundoApellido,
					nm_PrimerNombre = @IVch_nm_PrimerNombre,
					nm_OtrosNombres= @IVch_nm_OtrosNombres,
					Id_Pais = @IInt_Id_Pais,
					Id_TipoIdentificacion = @IIntId_TipoIdentificacion,
					cd_NumeroIdentificacion = @IVch_cd_NumeroIdentificacion,
					nm_CorreoElectronico = @IVch_nm_CorreoElectronico,
					dt_FechaIngreso = @IDt_FechaIngreso,
					Id_Area = @IInt_Id_Area,
					dt_Modificado = Cidenet_Util.fn_GetLocalDate()
				WHERE Id_Empleados = @IInt_Id_Empleados


			 END
		
		ELSE
			 BEGIN
				 SET @IVch_Mensaje = 'No Existe este empleado!'
			 END

			SELECT @IVch_Mensaje AS Result

	END TRY
	BEGIN CATCH
	INSERT Cidenet_Log.Logs (nm_SToreProcedure, nm_LineaError, nm_DescripcionError, dt_Creado, dt_Modificado)
	SELECT ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE(),Cidenet_Util.fn_GetLocalDate(),Cidenet_Util.fn_GetLocalDate()

	SET  @IVch_Mensaje = 'Ocurrio un error al insertar el Empleado, revisar Logs!'
	SELECT @IVch_Mensaje AS Result

	END CATCH

END
GO
/****** Object:  StoredProcedure [Cidenet_Empleado].[pr_GetOne_Empleado]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripción: Creación de SP
*************************************************************************************
** BITÁCORA:
*************************************************************************************
** Fecha:          Autor:					Descripción:
** 2021-08-18	   Edinson Paternina R		Creación Inicial
*************************************************************************************/

CREATE PROCEDURE [Cidenet_Empleado].[pr_GetOne_Empleado]
@cd_NumeroIdentificacion VARCHAR(100) = NULL
AS

BEGIN

	BEGIN TRY 

		IF((SELECT COUNT(1)  FROM Cidenet_Empleado.Empleados WHERE cd_NumeroIdentificacion = @cd_NumeroIdentificacion)>0)
			BEGIN
				select * from Cidenet_Empleado.Empleados where  cd_NumeroIdentificacion = @cd_NumeroIdentificacion
			END
		
			

	END TRY
	BEGIN CATCH
	INSERT Cidenet_Log.Logs (nm_SToreProcedure, nm_LineaError, nm_DescripcionError, dt_Creado, dt_Modificado)
	SELECT ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE(),Cidenet_Util.fn_GetLocalDate(),Cidenet_Util.fn_GetLocalDate()


	END CATCH

END
GO
/****** Object:  StoredProcedure [Cidenet_Maestro].[pr_Areas_Get]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  SP   Script Date: 17/08/2021 ******/
/************************************************************************************
** Descripción: Creación de SP
*************************************************************************************
** BITÁCORA:
*************************************************************************************
** Fecha:          Autor:					Descripción:
** 2021-08-17	   Edinson Paternina R		Creación Inicial
*************************************************************************************/

CREATE PROCEDURE [Cidenet_Maestro].[pr_Areas_Get]
AS

BEGIN

	SELECT Id_Areas AS Value,
	nm_Descripcion AS Description 
	FROM Cidenet_Maestro.Areas

END


GO
/****** Object:  StoredProcedure [Cidenet_Maestro].[pr_Paises_Get]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  SP   Script Date: 17/08/2021 ******/
/************************************************************************************
** Descripción: Creación de SP
*************************************************************************************
** BITÁCORA:
*************************************************************************************
** Fecha:          Autor:					Descripción:
** 2021-08-17	   Edinson Paternina R		Creación Inicial
*************************************************************************************/

CREATE PROCEDURE [Cidenet_Maestro].[pr_Paises_Get]
AS

BEGIN

	SELECT Id_Paises AS Value,
	nm_Descripcion AS Description 
	FROM Cidenet_Maestro.Paises

END

GO
/****** Object:  StoredProcedure [Cidenet_Maestro].[pr_TipoIdentificacion_Get]    Script Date: 19/08/2021 5:27:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  SP   Script Date: 17/08/2021 ******/
/************************************************************************************
** Descripción: Creación de SP
*************************************************************************************
** BITÁCORA:
*************************************************************************************
** Fecha:          Autor:					Descripción:
** 2021-08-17	   Edinson Paternina R		Creación Inicial
*************************************************************************************/

CREATE PROCEDURE [Cidenet_Maestro].[pr_TipoIdentificacion_Get]
AS

BEGIN

	SELECT Id_TipoDocumentos AS Value,
	nm_Descripcion AS Description 
	FROM Cidenet_Maestro.TipoDocumentos

END
GO
USE [master]
GO
ALTER DATABASE [DataBaseCidenet] SET  READ_WRITE 
GO
