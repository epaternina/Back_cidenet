USE [DataBaseCidenet]
GO

/****** Object:  StoredProcedure [Cidenet_Empleado].[pr_Empleados_Get]    Script Date: 19/08/2021 5:11:51 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripci�n: Creaci�n de SP
*************************************************************************************
** BIT�CORA:
*************************************************************************************
** Fecha:          Autor:					Descripci�n:
** 2021-08-19	   Edinson Paternina R		Creaci�n Inicial
*************************************************************************************/
CREATE PROCEDURE [Cidenet_Empleado].[pr_Empleados_Get]
@IVch_nm_PrimerApellido VARCHAR(100) = NULL,
@IVch_nm_SegundoApellido VARCHAR(100) = NULL,
@IVch_nm_PrimerNombre VARCHAR(100) = NULL,
@IVch_nm_OtrosNombres VARCHAR(100) = NULL,
@IIntId_TipoIdentificacion BIGINT = NULL,
@IVch_cd_NumeroIdentificacion VARCHAR(100) = NULL,
@IInt_Id_Pais BIGINT = NULL,
@IVch_nm_CorreoElectronico VARCHAR(100) = NULL,
@Ib_ind_Estado BIT = NULL,
--Paginaci�n
@PageNumber	INT = 1,
@PageSize	INT = 10

AS

BEGIN

--Secci�n de Declaraciones
DECLARE @vVch_Consulta NVARCHAR(MAX) = ''
DECLARE @vVch_WHERE NVARCHAR(MAX) = '  WHERE 1=1' 
DECLARE @vVch_Prametros NVARCHAR(MAX) = ''
DECLARE @vVch_ORDERBY NVARCHAR(MAX) = ''
DECLARE @vVch_FROM NVARCHAR(MAX) = ''

BEGIN TRY 

	IF(@IVch_nm_PrimerApellido IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.nm_PrimerApellido = @IVch_nm_PrimerApellido)'
		END

	IF(@IVch_nm_SegundoApellido IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.nm_SegundoApellido = @IVch_nm_SegundoApellido)'
		END

	IF(@IVch_nm_OtrosNombres IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.nm_OtrosNombres = @IVch_nm_OtrosNombres)'
		END

	IF(@IVch_nm_PrimerNombre IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.nm_PrimerNombre = @IVch_nm_PrimerNombre)'
		END

	IF(@IIntId_TipoIdentificacion IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.Id_TipoIdentificacion = @IIntId_TipoIdentificacion)'
		END

	IF(@IVch_cd_NumeroIdentificacion IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.cd_NumeroIdentificacion = @IVch_cd_NumeroIdentificacion)'
		END

	IF(@IInt_Id_Pais IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.Id_Pais = @IInt_Id_Pais)'
		END

	IF(@IVch_nm_CorreoElectronico IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.nm_CorreoElectronico = @IVch_nm_CorreoElectronico)'
		END

	IF(@Ib_ind_Estado IS NOT NULL)
		BEGIN		
			SET @vVch_WHERE = @vVch_WHERE + 'AND (E.ind_Estado = @Ib_ind_Estado)'
		END

		
	 SET  @vVch_Prametros = N'
							@IVch_nm_PrimerApellido VARCHAR(100) = NULL,
							@IVch_nm_SegundoApellido VARCHAR(100) = NULL,
							@IVch_nm_OtrosNombres VARCHAR(100) = NULL,
							@IVch_nm_PrimerNombre VARCHAR(100) = NULL,
							@IIntId_TipoIdentificacion BIGINT = NULL,
							@IVch_cd_NumeroIdentificacion VARCHAR(100) = NULL,
							@IInt_Id_Pais BIGINT = NULL,
							@IVch_nm_CorreoElectronico VARCHAR(100) = NULL,
							@Ib_ind_Estado BIT = NULL,
							@PageNumber INT = 1,
							@PageSize INT = 10'

	 SET @vVch_ORDERBY	  = 'ORDER BY  E.dt_Creado DESC
							 OFFSET (@PageNumber-1)*ISNULL(@PageSize,1) ROWS FETCH NEXT ISNULL(@PageSize,@@ROWCOUNT) ROWS ONLY'
	
	 SET @vVch_FROM		  = 'FROM Cidenet_Empleado.Empleados AS E 
							 INNER JOIN Cidenet_Maestro.Paises P  ON P.Id_Paises = E.Id_Pais
							 INNER JOIN Cidenet_Maestro.TipoDocumentos TD  ON TD.Id_TipoDocumentos = E.Id_TipoIdentificacion
							 INNER JOIN Cidenet_Maestro.Areas A  ON A.Id_Areas = E.Id_Area'

	 SET @vVch_Consulta	  ='SELECT 
							  E.Id_Empleados
							 ,E.nm_PrimerApellido
							 ,E.nm_SegundoApellido
							 ,E.nm_PrimerNombre
							 ,E.nm_OtrosNombres
							 ,P.nm_Descripcion AS nm_Pais
							 ,TD.nm_Descripcion AS cd_TipoIdentificacion
							 ,E.cd_NumeroIdentificacion
							 ,E.nm_CorreoElectronico
							 ,E.dt_FechaIngreso
							 ,A.nm_Descripcion AS nm_Area
							 ,E.ind_Estado
							 ,E.dt_FechaHoraRegistro
							 ,E.dt_Creado
							 ,TotalRecords = COUNT(1) OVER()' 
							 + @vVch_FROM +  @vVch_WHERE +  @vVch_ORDERBY

 EXECUTE sp_executesql
             @statement						= @vVch_Consulta
			,@params						= @vVch_Prametros
			,@IVch_nm_PrimerApellido		= @IVch_nm_PrimerApellido
			,@IVch_nm_SegundoApellido		= @IVch_nm_SegundoApellido
			,@IVch_nm_OtrosNombres			= @IVch_nm_OtrosNombres
			,@IVch_nm_PrimerNombre			= @IVch_nm_PrimerNombre
			,@IIntId_TipoIdentificacion		= @IIntId_TipoIdentificacion
			,@IVch_cd_NumeroIdentificacion	= @IVch_cd_NumeroIdentificacion
			,@IInt_Id_Pais					= @IInt_Id_Pais
			,@IVch_nm_CorreoElectronico		= @IVch_nm_CorreoElectronico
			,@Ib_ind_Estado					= @Ib_ind_Estado
			,@PageNumber 					= @PageNumber 
			,@PageSize						= @PageSize
END TRY
	BEGIN CATCH
		INSERT Cidenet_Log.Logs (nm_SToreProcedure, nm_LineaError, nm_DescripcionError, dt_Creado, dt_Modificado)
		SELECT ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE(),Cidenet_Util.fn_GetLocalDate(),Cidenet_Util.fn_GetLocalDate()


	END CATCH
	
END 

GO


