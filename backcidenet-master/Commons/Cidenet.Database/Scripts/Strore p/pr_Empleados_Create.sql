USE [DataBaseCidenet]
GO

/****** Object:  StoredProcedure [Cidenet_Empleado].[pr_Empleados_Create]    Script Date: 19/08/2021 5:10:44 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripci�n: Creaci�n de SP
*************************************************************************************
** BIT�CORA:
*************************************************************************************
** Fecha:          Autor:					Descripci�n:
** 2021-08-19	   Edinson Paternina R		Creaci�n Inicial
*************************************************************************************/
CREATE PROCEDURE [Cidenet_Empleado].[pr_Empleados_Create]
@IVch_nm_PrimerApellido VARCHAR(100) = NULL,
@IVch_nm_SegundoApellido VARCHAR(100) = NULL,
@IVch_nm_PrimerNombre VARCHAR(100) = NULL,
@IVch_nm_OtrosNombres VARCHAR(100) = NULL,
@IInt_Id_Pais BIGINT = NULL,
@IIntId_TipoIdentificacion BIGINT = NULL,
@IVch_cd_NumeroIdentificacion VARCHAR(100) = NULL,
@IDt_FechaIngreso DATETIME = NULL,
@IInt_Id_Area BIGINT = NULL
AS

BEGIN

	DECLARE @IVch_nm_CorreoElectronico VARCHAR (100) = ''
	DECLARE @IVch_Mensaje VARCHAR (100) = 'Registro Exitoso!'
	

	BEGIN TRY 

		IF((SELECT COUNT(1)  FROM Cidenet_Empleado.Empleados WHERE cd_NumeroIdentificacion = @IVch_cd_NumeroIdentificacion)=0 )
			BEGIN
				
				SET @IVch_nm_CorreoElectronico = (SELECT Cidenet_Empleado.fn_Correo_Get(@IVch_nm_PrimerApellido, @IVch_nm_PrimerNombre,@IInt_Id_Pais,ROUND(((99 - 2) * RAND() + 1), 0)))

						INSERT INTO Cidenet_Empleado.Empleados (nm_PrimerApellido,nm_SegundoApellido, nm_PrimerNombre, nm_OtrosNombres,
																Id_Pais, Id_TipoIdentificacion, cd_NumeroIdentificacion, nm_CorreoElectronico,
																dt_FechaIngreso, Id_Area, ind_Estado, dt_FechaHoraRegistro, dt_Creado, dt_Modificado)
														 VALUES(@IVch_nm_PrimerApellido, @IVch_nm_SegundoApellido, @IVch_nm_PrimerNombre, @IVch_nm_OtrosNombres,
																@IInt_Id_Pais, @IIntId_TipoIdentificacion, @IVch_cd_NumeroIdentificacion, @IVch_nm_CorreoElectronico,
																@IDt_FechaIngreso,@IInt_Id_Area,1, Cidenet_Util.fn_GetLocalDate(), Cidenet_Util.fn_GetLocalDate(), Cidenet_Util.fn_GetLocalDate())
			 END
		
		ELSE
			 BEGIN
				 SET @IVch_Mensaje = 'Ya Existe este empleado!'
			 END

			SELECT @IVch_Mensaje AS Result

	END TRY
	BEGIN CATCH
	INSERT Cidenet_Log.Logs (nm_SToreProcedure, nm_LineaError, nm_DescripcionError, dt_Creado, dt_Modificado)
	SELECT ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE(),Cidenet_Util.fn_GetLocalDate(),Cidenet_Util.fn_GetLocalDate()

	SET  @IVch_Mensaje = 'Ocurrio un error al insertar el Empleado, revisar Logs!'
	SELECT @IVch_Mensaje AS Result

	END CATCH

END

GO


