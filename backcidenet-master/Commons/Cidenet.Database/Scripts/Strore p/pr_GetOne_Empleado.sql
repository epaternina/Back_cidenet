USE [DataBaseCidenet]
GO

/****** Object:  StoredProcedure [Cidenet_Empleado].[pr_GetOne_Empleado]    Script Date: 19/08/2021 5:12:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripci�n: Creaci�n de SP
*************************************************************************************
** BIT�CORA:
*************************************************************************************
** Fecha:          Autor:					Descripci�n:
** 2021-08-19	   Edinson Paternina R		Creaci�n Inicial
*************************************************************************************/

CREATE PROCEDURE [Cidenet_Empleado].[pr_GetOne_Empleado]
@cd_NumeroIdentificacion VARCHAR(100) = NULL
AS

BEGIN

	BEGIN TRY 

		IF((SELECT COUNT(1)  FROM Cidenet_Empleado.Empleados WHERE cd_NumeroIdentificacion = @cd_NumeroIdentificacion)>0)
			BEGIN
				select * from Cidenet_Empleado.Empleados where  cd_NumeroIdentificacion = @cd_NumeroIdentificacion
			END
		
			

	END TRY
	BEGIN CATCH
	INSERT Cidenet_Log.Logs (nm_SToreProcedure, nm_LineaError, nm_DescripcionError, dt_Creado, dt_Modificado)
	SELECT ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE(),Cidenet_Util.fn_GetLocalDate(),Cidenet_Util.fn_GetLocalDate()


	END CATCH

END
GO


