USE [DataBaseCidenet]
GO

/****** Object:  StoredProcedure [Cidenet_Maestro].[pr_Paises_Get]    Script Date: 19/08/2021 5:13:44 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  SP   Script Date: 17/08/2021 ******/
/************************************************************************************
** Descripci�n: Creaci�n de SP
*************************************************************************************
** BIT�CORA:
*************************************************************************************
** Fecha:          Autor:					Descripci�n:
** 2021-08-19	   Edinson Paternina R		Creaci�n Inicial
*************************************************************************************/

CREATE PROCEDURE [Cidenet_Maestro].[pr_Paises_Get]
AS

BEGIN

	SELECT Id_Paises AS Value,
	nm_Descripcion AS Description 
	FROM Cidenet_Maestro.Paises

END

GO


