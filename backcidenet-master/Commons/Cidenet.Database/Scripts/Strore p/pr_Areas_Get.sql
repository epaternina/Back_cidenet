USE [DataBaseCidenet]
GO

/****** Object:  StoredProcedure [Cidenet_Maestro].[pr_Areas_Get]    Script Date: 19/08/2021 5:13:23 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  SP   Script Date: 17/08/2021 ******/
/************************************************************************************
** Descripci�n: Creaci�n de SP
*************************************************************************************
** BIT�CORA:
*************************************************************************************
** Fecha:          Autor:					Descripci�n:
** 2021-08-19	   Edinson Paternina R		Creaci�n Inicial
*************************************************************************************/

CREATE PROCEDURE [Cidenet_Maestro].[pr_Areas_Get]
AS

BEGIN

	SELECT Id_Areas AS Value,
	nm_Descripcion AS Description 
	FROM Cidenet_Maestro.Areas

END


GO


