USE [DataBaseCidenet]
GO

/****** Object:  StoredProcedure [Cidenet_Empleado].[pr_Empleados_Delete]    Script Date: 19/08/2021 5:11:21 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripci�n: Creaci�n de SP
*************************************************************************************
** BIT�CORA:
*************************************************************************************
** Fecha:          Autor:					Descripci�n:
** 2021-08-19	   Edinson Paternina R		Creaci�n Inicial
*************************************************************************************/

CREATE PROCEDURE [Cidenet_Empleado].[pr_Empleados_Delete]
@cd_NumeroIdentificacion VARCHAR(100) = NULL
AS

BEGIN

	DECLARE @IVch_Mensaje VARCHAR (100) = 'Empleado eliminado de forma Exitosa!'

	BEGIN TRY 

		IF((SELECT COUNT(1)  FROM Cidenet_Empleado.Empleados WHERE cd_NumeroIdentificacion = @cd_NumeroIdentificacion)>0)
			BEGIN
				DELETE FROM Cidenet_Empleado.Empleados WHERE cd_NumeroIdentificacion = @cd_NumeroIdentificacion
			END
		ELSE
			BEGIN
				SET @IVch_Mensaje = 'No Existe este empleado!'
			END

			SELECT @IVch_Mensaje AS Result

	END TRY
	BEGIN CATCH
	INSERT Cidenet_Log.Logs (nm_SToreProcedure, nm_LineaError, nm_DescripcionError, dt_Creado, dt_Modificado)
	SELECT ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE(),Cidenet_Util.fn_GetLocalDate(),Cidenet_Util.fn_GetLocalDate()

	SET  @IVch_Mensaje = 'Ocurrio un error al insertar el Empleado, revisar Logs!'
	SELECT @IVch_Mensaje AS Result

	END CATCH

END
GO


