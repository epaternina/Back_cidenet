USE [DataBaseCidenet]
GO

/****** Object:  StoredProcedure [Cidenet_Empleado].[pr_Empleados_Update]    Script Date: 19/08/2021 5:12:23 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  SP   Script Date: 16/08/2021 ******/
/************************************************************************************
** Descripción: Creación de SP
*************************************************************************************
** BITÁCORA:
*************************************************************************************
** Fecha:          Autor:					Descripción:
** 2021-08-19	   Edinson Paternina R		Creación Inicial
*************************************************************************************/


CREATE PROCEDURE [Cidenet_Empleado].[pr_Empleados_Update]
@IInt_Id_Empleados BIGINT = NULL,
@IVch_nm_PrimerApellido VARCHAR(100) = NULL,
@IVch_nm_SegundoApellido VARCHAR(100) = NULL,
@IVch_nm_PrimerNombre VARCHAR(100) = NULL,
@IVch_nm_OtrosNombres VARCHAR(100) = NULL,
@IInt_Id_Pais BIGINT = NULL,
@IIntId_TipoIdentificacion BIGINT = NULL,
@IVch_cd_NumeroIdentificacion VARCHAR(100) = NULL,
@IDt_FechaIngreso DATETIME = NULL,
@IInt_Id_Area BIGINT = NULL
AS

BEGIN

	DECLARE @IVch_nm_CorreoElectronico VARCHAR (100) = ''
	DECLARE @IVch_Mensaje VARCHAR (100) = 'Actualización Exitosa!'
	DECLARE @PrimerApellido VARCHAR (100) = ''
	DECLARE @PrimerNombre VARCHAR (100) = ''
	

	BEGIN TRY 

		IF((SELECT COUNT(1)  FROM Cidenet_Empleado.Empleados WHERE Id_Empleados = @IInt_Id_Empleados)>0 )
			BEGIN
				
				SET @PrimerApellido = ( SELECT TOP 1 nm_PrimerApellido FROM Cidenet_Empleado.Empleados WHERE Id_Empleados = @IInt_Id_Empleados)
				SET @PrimerNombre =	  ( SELECT TOP 1 nm_PrimerNombre FROM Cidenet_Empleado.Empleados WHERE Id_Empleados = @IInt_Id_Empleados)
				SET @IVch_nm_CorreoElectronico = (SELECT TOP 1 nm_CorreoElectronico FROM Cidenet_Empleado.Empleados WHERE Id_Empleados = @IInt_Id_Empleados)

				IF(@PrimerApellido!=@IVch_nm_PrimerApellido OR @PrimerNombre != @IVch_nm_PrimerNombre)
					BEGIN
						SET @IVch_nm_CorreoElectronico = (SELECT Cidenet_Empleado.fn_Correo_Get(@IVch_nm_PrimerApellido, @IVch_nm_PrimerNombre,@IInt_Id_Pais,ROUND(((99 - 2) * RAND() + 1), 0)))
					END

				UPDATE Cidenet_Empleado.Empleados
				SET nm_PrimerApellido= @IVch_nm_PrimerApellido,
					nm_SegundoApellido = @IVch_nm_SegundoApellido,
					nm_PrimerNombre = @IVch_nm_PrimerNombre,
					nm_OtrosNombres= @IVch_nm_OtrosNombres,
					Id_Pais = @IInt_Id_Pais,
					Id_TipoIdentificacion = @IIntId_TipoIdentificacion,
					cd_NumeroIdentificacion = @IVch_cd_NumeroIdentificacion,
					nm_CorreoElectronico = @IVch_nm_CorreoElectronico,
					dt_FechaIngreso = @IDt_FechaIngreso,
					Id_Area = @IInt_Id_Area,
					dt_Modificado = Cidenet_Util.fn_GetLocalDate()
				WHERE Id_Empleados = @IInt_Id_Empleados


			 END
		
		ELSE
			 BEGIN
				 SET @IVch_Mensaje = 'No Existe este empleado!'
			 END

			SELECT @IVch_Mensaje AS Result

	END TRY
	BEGIN CATCH
	INSERT Cidenet_Log.Logs (nm_SToreProcedure, nm_LineaError, nm_DescripcionError, dt_Creado, dt_Modificado)
	SELECT ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE(),Cidenet_Util.fn_GetLocalDate(),Cidenet_Util.fn_GetLocalDate()

	SET  @IVch_Mensaje = 'Ocurrio un error al insertar el Empleado, revisar Logs!'
	SELECT @IVch_Mensaje AS Result

	END CATCH

END
GO


