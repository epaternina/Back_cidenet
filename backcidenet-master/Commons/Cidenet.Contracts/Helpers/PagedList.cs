﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Cidenet.Contracts.Helpers
{
    [DataContract]
    public class PagedList<T>
    {
        private const long EMPTY_COLLECTION = -1;
        private readonly long _TotalItemCount = EMPTY_COLLECTION;

        public PagedList() { } // empty ctor for json deserialization

        public PagedList(IEnumerable<T> list, long? page = 1, long? pageSize = 0)
        {
            this.Items = list;
            this.PageNumber = page ?? 1;
            this.PageSize = pageSize ?? 0;
        }

        public PagedList(IEnumerable<T> list, long? page = 1, long? pageSize = 0, long? allItemsCount = EMPTY_COLLECTION)
        {
            this.Items = list;
            this.PageNumber = page ?? 1;
            this.PageSize = pageSize ?? 0;
            this._TotalItemCount = allItemsCount ?? EMPTY_COLLECTION;
        }

        public PagedList(IEnumerable<T> list, object additionalItems, long? page = 1, long? pageSize = 0, long? allItemsCount = EMPTY_COLLECTION)
        {
            this.Items = list;
            this.PageNumber = page ?? 1;
            this.PageSize = pageSize ?? 0;
            this._TotalItemCount = allItemsCount ?? EMPTY_COLLECTION;
            this.AdditionalItems = additionalItems;
        }

        [DataMember(Name = "items")]
        public IEnumerable<T> Items { get; set; }

        [DataMember(Name = "pageNumber")]
        public long PageNumber { get; set; }

        [DataMember(Name = "pageSize")]
        public long PageSize { get; set; }

        [DataMember(Name = "totalItemCount")]
        public long TotalItemCount
        {
            get
            {
                return _TotalItemCount >= 0 ? _TotalItemCount : (Items == null ? 0 : Items.Count());
            }
        }

        [DataMember(Name = "totalPages")]
        public long TotalPages
        {
            get
            {
                if (Items == null)
                { return 0; }
                else if (_TotalItemCount >= 0)
                { return (long)Math.Ceiling(_TotalItemCount / (double)PageSize); }
                else
                { return (long)Math.Ceiling((Items == null ? 0 : Items.Count()) / (double)PageSize); }
            }
        }

        [DataMember(Name = "additionalItems")]
        public object AdditionalItems { get; set; }
    }
}
