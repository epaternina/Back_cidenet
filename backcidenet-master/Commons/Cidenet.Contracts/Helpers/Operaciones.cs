﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace Cidenet.Contracts.Helpers
{
    public class Operaciones
    {
        public static void saveError(object obj, Exception ex)
        {
            string fecha = System.DateTime.Now.ToString("yyyyMMdd");
            string hora = System.DateTime.Now.ToString("HH:mm:ss");
            string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location.Substring(0, Assembly.GetEntryAssembly().Location.IndexOf("bin\\")))+ "\\logs\\";
            path = @path + fecha + ".txt";

            try
            {
                StreamWriter sw;

                if (File.Exists(path))
                {
                   sw = new StreamWriter(path, true);
                }
                else
                {
                    var myFile = File.Create(path);
                    myFile.Close();

                    sw = new StreamWriter(path, true);
                }

                StackTrace stacktrace = new StackTrace();
                sw.WriteLine(obj.GetType().FullName + " " + hora);
                sw.WriteLine(stacktrace.GetFrame(1).GetMethod().Name + " - " + ex.Message + "-" + ex.StackTrace.ToString());
                sw.WriteLine("");

                sw.Flush();
                sw.Close();

            }
            catch (Exception e) {

                throw;
            }
            

            

            
        }
    }
}
